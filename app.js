require('dotenv').config();
const express = require('express');
const app = express();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const bodyParser = require('body-parser');
const steamLogin = require('steam-login');
const mongoose = require('mongoose');

const steam = require('./lib/steam');

const port = process.env.PORT || 5000;

var online = 0;

// middleware
app.set('view engine', 'ejs');
app.set('port', port);
app.use(bodyParser.urlencoded({ extended: false }));
app.use(require('express-session')({ resave: true, saveUninitialized: true, secret: 'a secret' }));
app.use(steamLogin.middleware({
    realm: 'http://localhost:5000/',
    verify: 'http://localhost:5000/verify',
    apiKey: process.env.STEAM_API || 'EB2326294BBEC32DA19D6DF980B900BB'
}));

mongoose.connect(process.env.MONGOOSE_URL, {useNewUrlParser: true, useUnifiedTopology: true});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
    console.log('[db] connected');
});

global.steam = steamLogin;
global.io = io;
global.db = db;

//steam.auth();

// require the routes
const indexRoute = require('./routes/index');
const steamRoute = require('./routes/steam');

// setup the routes
app.use('/', indexRoute);
app.use('/', steamRoute);

// socket
io.on('connection', (socket) => {
    online++;

    socket.on('disconnect', () => {
        online--;
    });
});

setInterval(() => {
    io.emit('online', online);
}, 3000);

// set the app to listen on the port
http.listen(port, () => {
    console.log(`Server running on port: ${port}`)
})