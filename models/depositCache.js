const mongoose = require('mongoose');

const depositCacheSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    user: Number, //steamid64
    items: Array,
    generated: Date
});

module.exports = mongoose.model('DepositCache', depositCacheSchema)