const express = require('express');
const router = express.Router();
const steamInventory = require('get-steam-inventory');
const mongoose = require('mongoose');

const DepositCache = require('../models/depositCache.js');

const steam = require('../lib/steam');

router.get('/', (req, res, next) => {
    res.render('index', {
        child: 'home',
        user: req.user
    })
})

router.get('/deposit', async (req, res, next) => {
    if(req.user) {
        const instance = await DepositCache.findOne({user: req.user.steamid});
        var inventory = [];
        if(instance) {
            inventory = instance.items;
        } else {
            var inventory = await steam.getUserInventory(req.user.steamid).then(data => {return data}, e => {return []});
        
            var cache = new DepositCache({
                _id: new mongoose.Types.ObjectId,
                user: req.user.steamid,
                items: inventory,
                generated: new Date()
            });
            cache.save(err => {if(err) console.log(err)})
        }
    
        res.render('index', {
            child: 'deposit',
            user: req.user,
            inventory: inventory
        });
    } else {
        // before access to deposit section he gotta login
        res.redirect('/authenticate');
    }
})

router.get('/deposit/refresh', async (req, res, next) => {
    if(req.user) {
        let inventory = await steam.getUserInventory(req.user.steamid).then(data => {return data}, e => {return []})
        DepositCache.updateOne({ user: req.user.steamid }, { items: inventory }, (err) => { if(err) throw err} );

        res.redirect('/deposit');
    } else {
        res.redirect('/authenticate');
    }
})

module.exports = router;