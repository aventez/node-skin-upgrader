const express = require('express');
const router = express.Router();

router.get('/authenticate', steam.authenticate(), (req, res) => {
    res.redirect('/');
});

router.get('/verify', steam.verify(), function(req, res) {
    res.redirect('/');
});

router.get('/logout', steam.enforceLogin('/'), function(req, res) {
    req.logout();
    res.redirect('/');
})

module.exports = router;